import jetbrains.buildServer.configs.kotlin.v2019_2.*
import jetbrains.buildServer.configs.kotlin.v2019_2.buildSteps.dockerCommand
import jetbrains.buildServer.configs.kotlin.v2019_2.buildSteps.exec
import jetbrains.buildServer.configs.kotlin.v2019_2.buildSteps.script
import jetbrains.buildServer.configs.kotlin.v2019_2.projectFeatures.gitlabConnection
import jetbrains.buildServer.configs.kotlin.v2019_2.triggers.vcs

/*
The settings script is an entry point for defining a TeamCity
project hierarchy. The script should contain a single call to the
project() function with a Project instance or an init function as
an argument.

VcsRoots, BuildTypes, Templates, and subprojects can be
registered inside the project using the vcsRoot(), buildType(),
template(), and subProject() methods respectively.

To debug settings scripts in command-line, run the

    mvnDebug org.jetbrains.teamcity:teamcity-configs-maven-plugin:generate

command and attach your debugger to the port 8000.

To debug in IntelliJ Idea, open the 'Maven Projects' tool window (View
-> Tool Windows -> Maven Projects), find the generate task node
(Plugins -> teamcity-configs -> teamcity-configs:generate), the
'Debug' option is available in the context menu for the task.
*/

version = "2020.1"

project {

    buildType(UnitTest)
    buildType(DeployToStaging)
    buildType(DeployScript)
    buildType(DeployToProduction)
    buildType(Build)

    template(HmoApp)
    template(Deploy)

    features {
        gitlabConnection {
            id = "PROJECT_EXT_3"
            displayName = "GitLab.com"
            applicationId = "698ff70ef0b19fcff6c92a4fec623847d3c20a8e1bbb618127dacf5e4ebc1b22"
            clientSecret = "credentialsJSON:f62bb71e-c646-4c2a-aa5e-60750731dd36"
        }
    }
}

object Build : BuildType({
    templates(HmoApp)
    name = "Build"

    dependencies {
        snapshot(UnitTest) {
        }
    }
    
    disableSettings("RUNNER_1", "RUNNER_4", "TRIGGER_1")
})

object DeployScript : BuildType({
    templates(Deploy)
    name = "Deploy Script"
    
    disableSettings("vcsTrigger")
})

object DeployToProduction : BuildType({
    templates(Deploy)
    name = "Deploy to Production"
    description = "Deploys App to Production"

    params {
        param("env.DEPLOYENV", "production")
    }

    dependencies {
        snapshot(Build) {
        }
    }
    
    disableSettings("vcsTrigger")
})

object DeployToStaging : BuildType({
    templates(Deploy)
    name = "Deploy to Staging"
    description = "Deploys App to Staging"

    params {
        param("env.DEPLOYENV", "staging")
    }

    dependencies {
        snapshot(Build) {
        }
    }
    
    disableSettings("vcsTrigger")
})

object UnitTest : BuildType({
    name = "Unit Test"

    params {
        param("env.WELCOMEMESSAGE", "Hello World!")
    }

    vcs {
        root(DslContext.settingsRoot)
    }

    steps {
        script {
            scriptContent = "./unit-tests.sh"
        }
    }

    triggers {
        vcs {
            enabled = false
        }
    }
})

object Deploy : Template({
    name = "deploy"

    params {
        param("env.DEPLOYENV", "")
    }

    vcs {
        root(DslContext.settingsRoot)
    }

    steps {
        script {
            id = "RUNNER_6"
            scriptContent = "./sim-deploy.sh"
        }
    }

    triggers {
        vcs {
            id = "vcsTrigger"
        }
    }
})

object HmoApp : Template({
    name = "hmo-app"

    params {
        param("welcomemessage", "test")
    }

    vcs {
        root(DslContext.settingsRoot)
    }

    steps {
        exec {
            name = "Simulated Unit Tests"
            id = "RUNNER_1"
            path = "unit-tests.sh"
            arguments = "welcomemessage"
        }
        dockerCommand {
            id = "RUNNER_2"
            commandType = build {
                source = file {
                    path = "apps/front-nginx/Dockerfile"
                }
            }
        }
        dockerCommand {
            id = "RUNNER_3"
            commandType = build {
                source = file {
                    path = "apps/frontend-hmo/Dockerfile"
                }
            }
        }
        dockerCommand {
            id = "RUNNER_4"
            executionMode = BuildStep.ExecutionMode.RUN_ON_FAILURE
            commandType = build {
                source = file {
                    path = "apps/hmo-solver/Dockerfile"
                }
                commandArgs = "--pull"
            }
        }
    }

    triggers {
        vcs {
            id = "TRIGGER_1"
        }
    }
})
