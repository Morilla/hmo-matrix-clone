#!/bin/bash
kubectl config use-context microk8s

KUBEADDR=$(multipass list --format json | jq '.list[]  | select(.name == "microk8s-vm") | .ipv4[0]' | sed 's/\"//g') CI_REGISTRY_IMAGE=${KUBEADDR}:32000 ./registry-start.sh
CI_REGISTRY_IMAGE=localhost:32000 ./render.sh

DEPLOYMENT_ENVIRONMENT="development"

export RELEASE_NAME="hmo-app-${DEPLOYMENT_ENVIRONMENT}"

helm install ${RELEASE_NAME} helm/build/hmo-app
if [ $? -ne 0 ]; then
    echo "helm install failed, likely already installed, attempting upgrade"
    helm upgrade ${RELEASE_NAME} helm/build/hmo-app
fi

exit 0
