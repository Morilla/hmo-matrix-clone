from . import app
from hmo_solver.app.utils import validate_applicants_dict
from hmo_solver.app import solver

from flask import request, make_response
from flask_cors import cross_origin

import csv
import json
from io import StringIO
from datetime import datetime


@app.route("/status")
@cross_origin()
def health():
    return "OK"


@app.route("/api/solve_dependents", methods=["POST"])
@cross_origin()
def post_solve_dependents():
    content = request.get_json()

    if validate_applicants_dict(content):
        return json.dumps(solver.group_dependents(content))


@app.route("/api/suggest_plan", methods=["POST"])
@cross_origin()
def post_suggest_plan():
    content = request.get_json()

    if validate_applicants_dict(content):
        return json.dumps(solver.suggest_plan(content))


@app.route("/api/compute_plan_price", methods=["POST"])
@cross_origin()
def post_compute_plan_price():
    content = request.get_json()
    return json.dumps(solver.compute_plan_price(content))


@app.route("/api/save_applications", methods=["POST"])
@cross_origin()
def post_save_applications():
    content = request.get_json()
    return json.dumps(solver.save_applications(content))


@app.route("/api/get_applications", methods=["GET"])
def get_applications():
    # TODO: get filters via query arguments
    return json.dumps(solver.get_applications())


@app.route("/api/export/applications", methods=["GET"])
def export_applications():
    # TODO: get filters via query arguments

    csv_data = solver.generate_csv()

    si = StringIO()
    cw = csv.writer(si)
    cw.writerows(csv_data)
    output = make_response(si.getvalue())
    output.headers["Content-Disposition"] = "attachment; filename=applications_{}.csv".format(datetime.now().strftime("%Y-%m-%d"))
    output.headers["Content-type"] = "text/csv"
    return output

