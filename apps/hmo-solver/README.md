# HMO Plan Solver

Suggests a plan based on the given applicant data. Determines whether applicant should be a dependent or applied as a principal account. Currently does not optimize based on a given budget, gives all principal accounts the highest plan available and gives cost estimate based on that.

## Data Format

### Sample Data

A `json` version of the sample data is also available on the repository for easy testing.

```python
test = {
    "employee": {
        "name": "Employee",
        "birthday": "2/13/1995",
        "employed": True,
        "married": True,
    },
    "dependents": [
        {
            "name": "Waif",
            "birthday": "24/8/1994",
            "relationship": "Spouse",
            "employed": True,
        },
        {
            "name": "Child",
            "birthday": "6/6/2006",
            "relationship": "Child",
            "employed": True,
        },
    ],
}
```

## Running the Solver

You can run the solver directly or instantiate a Docker container (run by Flask) with endpoints mapped to solver functions.

### Creating the Docker container

```bash
docker build . --tag hmo-solver:1.0.0
docker run -p 5050:5000 hmo-solver:1.0.0
```

The port configuration makes the application available at `localhost:5050`.

### Querying the Solver

**Sample Query:**

```bash
 curl -X POST -H "Content-Type: application/json" -d '{"employee":{"name":"Employee","birthday":"1995-02-13","employed":true,"married":true},"dependents":[{"name":"Waif","birthday":"08-24-1994","relationship":"Spouse","employed":true},{"name":"Child","birthday":"2006-06-06","relationship":"Child","employed":true}]}' localhost:5050/api/solve_dependents

curl -X POST -H "Content-Type: application/json" -d '{"employee":{"name":"Employee","birthday":"1995-02-13","employed":true,"married":true},"dependents":[{"name":"Waif","birthday":"1994-08-24","relationship":"Spouse","employed":true},{"name":"Child","birthday":"2006-06-06","relationship":"Child","employed":true}]}' http://hmo.opswerks.local:32160/api/suggest_plan

curl -X POST -H "Content-Type: application/json" -d '{"employee":{"name":"Employee","birthday":"1995-02-13","employed":true,"married":true},"dependents":[]}' http://hmo.opswerks.local:32160/api/suggest_plan

curl -X POST -H "Content-Type: application/json" -d '[{"planType":"Suite","primary":[{"name":"Employee Name","birthday":"1995-02-13","employed":true,"married":false}],"dependents":[],"id":1}]' http://hmo.opswerks.local:32160/api/compute_plan_price

```

## Working Endpoints

- `/status` [GET]
  - should return OK if server is configured and working properly
- `/api/solve_dependents` [POST]
  - Receives a `json` with applicant data. Returns list of applicants eligible to be dependents under the employee's principal account and which should be applied with their own principal accounts.
- `/api/suggest_plan` [POST]
  - Gives the suggested plan configuration based on the PeopleOps provided matrix
- `/api/compute_plan_price` [POST]
  - Computes for the price of the given application setups