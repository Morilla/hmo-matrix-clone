# hmo-matrix-collection

---

# Solver Kubernetes Manifests

## Notes

If you're running microk8s locally on MacOS, you are most likely running it inside another VM (usually multipass). This means the microk8s registry is *not* accessible via `localhost:32000`. You have to get the microk8s IP inside multipass.

```bash
multipass list

Name                    State             IPv4             Image
microk8s-vm             Running           192.168.64.2     Ubuntu 18.04 LTS
```

### Pushing images to the microk8s registry
```bash
docker tag hmo-solver:1.0.0 192.168.64.2:32000/hmo-solver:latest
docker push 192.168.64.2:32000/hmo-solver:latest
```

### Writing Templated Kubernetes Manifests

Sometimes, you want to use templated variables on your manifests to make it easy to deploy across different environments (e.g. changing the Docker registry address). To support this, basic templating features are supported using the `envsubst` command.

#### Example

We want to template the Docker registry address to make it easy to deploy locally and on the cloud:
```yaml
    spec:
      containers:
        - image: ${REGISTRY_ADDR}/image:latest
          name: image
          ports:
            - containerPort: 5000
```

We save this in `templates/manifest.yaml` then run `./render.sh`. If the `REGISTRY_ADDR` environment variable is set, the script will use the existing value, otherwise it will default to the value explicitly set in the script.

Given the `REGISTRY_ADDR` is set to `localhost:32000`, the rendered template should appear as `manifests/manifest.yaml` and look like this:
```yaml
    spec:
      containers:
        - image: localhost:32000/image:latest
          name: image
          ports:
            - containerPort: 5000
```

## Common Issues

### Server gave HTTP response to HTTPS client

You need to add the target docker registry to your client docker's list of insecure registries.

On MacOS, open your Docker app > Preferences > Docker Engine and add this to the configuration:
```json
"insecure-registries": ["xxx.xxx.xx.x:xxxxx"]
```
